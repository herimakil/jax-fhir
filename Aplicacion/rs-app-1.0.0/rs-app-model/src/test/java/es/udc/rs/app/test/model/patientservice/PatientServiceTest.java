package es.udc.rs.app.test.model.patientservice;

import static es.udc.rs.app.model.util.ModelConstants.EVENT_DATA_SOURCE;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.BeforeClass;
import org.junit.Test;

import es.udc.rs.app.exceptions.InputValidationException;
import es.udc.rs.app.exceptions.InstanceNotFoundException;
import es.udc.rs.app.model.clinicservice.ClinicService;
import es.udc.rs.app.model.clinicservice.ClinicServiceFactory;
import es.udc.rs.app.model.patient.Patient;
import es.udc.rs.app.model.patient.SqlPatientDao;
import es.udc.rs.app.model.patient.SqlPatientDaoFactory;
import es.udc.rs.app.sql.DataSourceLocator;
import es.udc.rs.app.sql.SimpleDataSource;


public class PatientServiceTest {
	
	private final long NON_EXISTENT_EVENT_ID = 999999;

	private static ClinicService clinicService = null;
	private static SqlPatientDao patientDao = null;
	
	@BeforeClass
	public static void init() {

		/*
		 * Create a simple data source and add it to "DataSourceLocator" (this
		 * is needed to test
		 * "es.udc.ws.app.model.patient.patientservice.clinicService"
		 */
		DataSource dataSource = new SimpleDataSource();

		/* Add "dataSource" to "DataSourceLocator". */
		DataSourceLocator.addDataSource(EVENT_DATA_SOURCE, dataSource);

		clinicService = ClinicServiceFactory.getService();
		patientDao = SqlPatientDaoFactory.getDao();
	}
	
	private Patient getValidPatient() {
		String name = "test valid patient";

		return new Patient(name);
	}
	
	private Patient createPatient(Patient patient) throws InputValidationException {

		Patient addedPatient = null;
		try{
			addedPatient = clinicService.addPatient(patient);
		}catch (InputValidationException e) {
			throw new RuntimeException(e);
		}
		
		return addedPatient;
	}

	private void updatePatient(Patient patient) {

		DataSource dataSource = DataSourceLocator
				.getDataSource(EVENT_DATA_SOURCE);

		try (Connection connection = dataSource.getConnection()) {

			try {

				/* Prepare connection. */
				connection
						.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
				connection.setAutoCommit(false);

				/* Do work. */
				patientDao.update(connection, patient);

				/* Commit. */
				connection.commit();

			} catch (InstanceNotFoundException e) {
				connection.commit();
				throw new RuntimeException(e);
			} catch (SQLException e) {
				connection.rollback();
				throw new RuntimeException(e);
			} catch (RuntimeException | Error e) {
				connection.rollback();
				throw e;
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	private void removePatient(Long patientId) throws InstanceNotFoundException {
		DataSource dataSource = DataSourceLocator
				.getDataSource(EVENT_DATA_SOURCE);
		try (Connection connection = dataSource.getConnection()) {
			try {
				connection
						.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
				connection.setAutoCommit(false);
				patientDao.remove(connection, patientId);
				connection.commit();
			} catch (InstanceNotFoundException e) {
				connection.commit();
				throw new RuntimeException(e);
			} catch (SQLException e) {
				connection.rollback();
				throw new RuntimeException(e);
			} catch (RuntimeException | Error e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}
	
	@Test
	public void testAddPatientAndFindPatient() throws InstanceNotFoundException, InputValidationException {
		Patient patient = getValidPatient();
		Patient addedPatient = null;

		addedPatient = clinicService.addPatient(patient);
		Patient foundPatient = clinicService.findPatient(addedPatient.getPatientId());
		assertEquals(addedPatient, foundPatient);

		removePatient(addedPatient.getPatientId());
	}

	@Test
	public void testAddInvalidPatient() throws InputValidationException, 
		InstanceNotFoundException {

		Patient patient = getValidPatient();
		Patient addedPatient = null;
		boolean exceptionCatched = false;

		try {

			// Check patient name not null
			patient.setName(null);
			try {
				addedPatient = clinicService.addPatient(patient);
			} catch (InputValidationException e) {
				exceptionCatched = true;
			}
			assertTrue(exceptionCatched);

			// Check patient name not empty
			exceptionCatched = false;
			patient = getValidPatient();
			patient.setName("");
			try {
				addedPatient = clinicService.addPatient(patient);
			} catch (InputValidationException e) {
				exceptionCatched = true;
			}
			assertTrue(exceptionCatched);

		} finally {
			if (!exceptionCatched) {
				// Clear Database

				removePatient(addedPatient.getPatientId());

			}
		}

	}

	@Test(expected = InstanceNotFoundException.class)
	public void testFindNonExistentPatient() throws InstanceNotFoundException {
		clinicService.findPatient(NON_EXISTENT_EVENT_ID);
	}

	@Test
	public void testUpdatePatient() throws InputValidationException,
			InstanceNotFoundException {

		Patient patient = createPatient(getValidPatient());
		try {

			patient.setName("New patient");

			clinicService.updatePatient(patient);

			Patient updatedPatient = clinicService.findPatient(patient.getPatientId());
			assertEquals(patient, updatedPatient);

		} finally {
			// Clear Database
			removePatient(patient.getPatientId());
		}

	}

	@Test(expected = InputValidationException.class)
	public void testUpdateInvalidPatient() throws InputValidationException,
			InstanceNotFoundException{

		Patient patient = createPatient(getValidPatient());
		try {
			// Check patient title not null
			patient = clinicService.findPatient(patient.getPatientId());
			patient.setName(null);
			clinicService.updatePatient(patient);
		} finally {
			// Clear Database
			removePatient(patient.getPatientId());
		}
	}

	@Test(expected = InstanceNotFoundException.class)
	public void testUpdateNonExistentPatient() throws InputValidationException,
			InstanceNotFoundException {
		Patient patient = getValidPatient();
		patient.setPatientId(NON_EXISTENT_EVENT_ID);
		clinicService.updatePatient(patient);
	}

	@Test(expected = InstanceNotFoundException.class)
	public void testRemovePatient() throws InstanceNotFoundException, InputValidationException {

		Patient patient = createPatient(getValidPatient());
		boolean exceptionCatched = false;
		try {
			clinicService.removePatient(patient.getPatientId());
		} catch (InstanceNotFoundException e) {
			exceptionCatched = true;
		}
		assertTrue(!exceptionCatched);

		clinicService.findPatient(patient.getPatientId());
	}

	@Test(expected = InstanceNotFoundException.class)
	public void testRemoveNonExistentPatient() throws InstanceNotFoundException {

		clinicService.removePatient(NON_EXISTENT_EVENT_ID);
	}

}
