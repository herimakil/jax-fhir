package es.udc.rs.app.model.util;

public class Contact{
	
	public enum Relationship {
		husband,wife,son,daughter,
		sister,brother,mother,father,
		grandmother,grandfather,grandson,granddaughter,
		uncle,aunt,counsin
	}
	private Relationship relationship;
	
//    private enum System { 	phone, fax, email, pager, url, sms, other };
//    private System system;

//    private Integer rank;

    private String value;

//    private enum Use { home, work, temp, old, mobile };
//    private Use use;
    
//    private Period period;
    
	public Contact(String relationship, String value) {
		super();
		this.relationship = Relationship.valueOf(relationship);
		this.value = value;
	}

	public Relationship getRelationship() {
		return relationship;
	}

	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((relationship == null) ? 0 : relationship.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (relationship != other.relationship)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Contact [relationship=" + relationship + ", value=" + value + "]";
	}

	

}
