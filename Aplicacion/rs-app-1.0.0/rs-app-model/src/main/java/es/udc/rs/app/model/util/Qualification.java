package es.udc.rs.app.model.util;

public class Qualification {
	
	private Identifier identifier;
	
	private enum Code { AA, AAS, ABA, AE, AS, BA, BBA, BE, 
		BFA, BN, BS, BSL, BSN, BT, CANP, CER, CMA, CNM, CNP, 
		CNS, CPNP, CRN, CTR, DBA, DED, DIP, DO, EMT, EMTP, 
		FPNP, HS, JD, MA, MBA, MCE, MD, MDA, MDI, ME, MED, 
		MEE, MFA, MME, MS, MSL, MSN, MT, MTH, NG, NP, PA, 
		PHD, PHE, PHS, PN, PharmD, RMA, RN, RPH, SEC, TS
	};
	private Code code;
	
	private Period period;
	
	private Organization issuer;

	public Qualification(Identifier identifier, String code, Period period, Organization issuer) {
		super();
		this.identifier = identifier;
		this.code = Qualification.Code.valueOf(code);
		this.period = period;
		this.issuer = issuer;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Organization getIssuer() {
		return issuer;
	}

	public void setIssuer(Organization issuer) {
		this.issuer = issuer;
	}

	@Override
	public String toString() {
		return "Qualification [identifier=" + identifier + ", code=" + code + ", period=" + period + ", issuer="
				+ issuer + "]";
	}
	
}
