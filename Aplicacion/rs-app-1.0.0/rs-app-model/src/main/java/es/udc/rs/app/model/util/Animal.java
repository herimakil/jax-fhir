package es.udc.rs.app.model.util;

import java.util.List;

public class Animal {
	private enum Species {
		dog,cat,cow,horse,bird,reptile,fish
	};
	private Species species;
	private enum Breeds {
		golden_retriever,siames,russian_blue,gold_fish,clown_fish
	};
	private List<Breeds> breeds;
	private enum GenderStatus {
		neutered,intact
	};
	private GenderStatus genderStatus;
	
	public Animal(String species, List<String> breeds, String genderStatus){
		this.species = Animal.Species.valueOf(species);
		for(String breed: breeds){
			this.breeds.add(Animal.Breeds.valueOf(breed));
		}
		this.genderStatus = Animal.GenderStatus.valueOf(genderStatus);
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public List<Breeds> getBreeds() {
		return breeds;
	}

	public void setBreeds(List<Breeds> breeds) {
		this.breeds = breeds;
	}

	public GenderStatus getGenderStatus() {
		return genderStatus;
	}

	public void setGenderStatus(GenderStatus genderStatus) {
		this.genderStatus = genderStatus;
	}
	
}
