package es.udc.rs.app.model.patient;

import java.util.Date;
import java.util.List;

import es.udc.rs.app.model.util.HumanName;
import es.udc.rs.app.model.util.Identifier;
import es.udc.rs.app.model.util.Communication;
import es.udc.rs.app.model.util.Contact;
import es.udc.rs.app.model.util.ContactPoint;
import es.udc.rs.app.model.util.Gender;

public class Patient {
	
	private Identifier identifier;
	
	private HumanName name;
	
	private boolean active;
	
	private ContactPoint telecom;
	
	private Gender gender;
	
	private Date birthDate;
	
	private boolean deceasedBoolean;
//	private Date deceasedDateTime;
	
//	private enum MaritalStatus{ A,D,I,L,M,P,S,T,U,W,UNK };
//	private MaritalStatus maritalStatus;
	
	private List<Contact> contact;
	
//	private List<Communication> communication;

	public Patient(Identifier identifier, HumanName name, boolean active, ContactPoint telecom, Gender gender,
			Date birthDate, Boolean deceasedBoolean, List<Contact> contact) {
		super();
		this.identifier = identifier;
		this.name = name;
		this.active = active;
		this.telecom = telecom;
		this.gender = gender;
		this.birthDate = birthDate;
		this.deceasedBoolean = deceasedBoolean;
		this.contact = contact;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public HumanName getName() {
		return name;
	}

	public void setName(HumanName name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ContactPoint getTelecom() {
		return telecom;
	}

	public void setTelecom(ContactPoint telecom) {
		this.telecom = telecom;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Boolean getDeceasedBoolean() {
		return deceasedBoolean;
	}

	public void setDeceasedBoolean(Boolean deceasedBoolean) {
		this.deceasedBoolean = deceasedBoolean;
	}

	public List<Contact> getContact() {
		return contact;
	}

	public void setContact(List<Contact> contact) {
		this.contact = contact;
	}
	
}
