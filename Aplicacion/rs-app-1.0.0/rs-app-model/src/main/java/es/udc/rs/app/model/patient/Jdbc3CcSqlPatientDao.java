package es.udc.rs.app.model.patient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;



public class Jdbc3CcSqlPatientDao extends AbstractSqlPatientDao{
	
	@Override
	public Patient create(Connection connection, Patient patient) {
			/* Create "queryString". */
	
			String queryString = "INSERT INTO Patient"
					+ " (name)"
					+ " VALUES (?)";
			

	        try (PreparedStatement preparedStatement = connection.prepareStatement(
	                        queryString, Statement.RETURN_GENERATED_KEYS)) {

	            /* Fill "preparedStatement". */
	            int i = 1;
	            preparedStatement.setString(i++, patient.getName());
	            

	            /* Execute query. */
	            preparedStatement.executeUpdate();

	            /* Get generated identifier. */
	            ResultSet resultSet = preparedStatement.getGeneratedKeys();

	            if (!resultSet.next()) {
	                throw new SQLException(
	                        "JDBC driver did not return generated key.");
	            }
	            Long patientId = resultSet.getLong(1);

	            /* Return attendance. */
	            
	            return new Patient(patientId, patient.getName());

	        } catch (SQLException e) {
	            throw new RuntimeException(e);
	        }
	}

}
