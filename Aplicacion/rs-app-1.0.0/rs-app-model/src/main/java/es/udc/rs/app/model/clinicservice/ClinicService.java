package es.udc.rs.app.model.clinicservice;


import es.udc.rs.app.exceptions.InputValidationException;
import es.udc.rs.app.exceptions.InstanceNotFoundException;
import es.udc.rs.app.model.patient.Patient;


public interface ClinicService {
	
	public Patient addPatient(Patient patient) throws InputValidationException;

    public void updatePatient(Patient patient) throws InstanceNotFoundException, InputValidationException;

	public Patient findPatient(String patientId) throws InstanceNotFoundException;

	void removePatient(String patientId) throws InstanceNotFoundException;

}
