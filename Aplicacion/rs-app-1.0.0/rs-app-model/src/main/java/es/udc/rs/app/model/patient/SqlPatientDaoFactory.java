package es.udc.rs.app.model.patient;

import es.udc.rs.app.configuration.ConfigurationParametersManager;

public class SqlPatientDaoFactory {
	
	private final static String CLASS_NAME_PARAMETER = "SqlPatientDaoFactory.className";
	private static SqlPatientDao dao = null;

	private SqlPatientDaoFactory() {
	}

	@SuppressWarnings("rawtypes")
	private static SqlPatientDao getInstance() {
		try {
			String daoClassName = ConfigurationParametersManager
					.getParameter(CLASS_NAME_PARAMETER);
			Class daoClass = Class.forName(daoClassName);
			return (SqlPatientDao) daoClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public synchronized static SqlPatientDao getDao() {

		if (dao == null) {
			dao = getInstance();
		}
		return dao;

	}
}
