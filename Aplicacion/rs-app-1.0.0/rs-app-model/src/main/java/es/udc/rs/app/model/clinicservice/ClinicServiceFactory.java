package es.udc.rs.app.model.clinicservice;

import es.udc.rs.app.configuration.ConfigurationParametersManager;

public class ClinicServiceFactory {
	
	
	private final static String CLASS_NAME_PARAMETER = "ClinicServiceFactory.className";
    private static ClinicService service = null;

    private ClinicServiceFactory() {
    }

    @SuppressWarnings("rawtypes")
    private static ClinicService getInstance() {
        try {
            String serviceClassName = ConfigurationParametersManager
                    .getParameter(CLASS_NAME_PARAMETER);
            Class serviceClass = Class.forName(serviceClassName);
            return (ClinicService) serviceClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized static ClinicService getService() {

        if (service == null) {
            service = getInstance();
        }
        return service;

    }
}
