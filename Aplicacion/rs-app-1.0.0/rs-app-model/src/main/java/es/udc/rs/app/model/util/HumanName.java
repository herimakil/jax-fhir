package es.udc.rs.app.model.util;

import java.util.List;

public class HumanName {
//	private enum Use { usual,official,temp,nickname,anonymous,old,maiden };
//	private Use use;
	
	private String text;
	private String family;
	private List<String> given;
	
//	private String prefix;
//	private String sufix;
//	private Period period;
	
	public HumanName(String family, List<String> given) {
		super();
		this.family = family;
		this.given = given;
		
		for (String name : given){
			this.text = this.text + name + " ";
		}
		this.text = this.text + family;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public List<String> getGiven() {
		return given;
	}
	public void setGiven(List<String> given) {
		this.given = given;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((family == null) ? 0 : family.hashCode());
		result = prime * result + ((given == null) ? 0 : given.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HumanName other = (HumanName) obj;
		if (family == null) {
			if (other.family != null)
				return false;
		} else if (!family.equals(other.family))
			return false;
		if (given == null) {
			if (other.given != null)
				return false;
		} else if (!given.equals(other.given))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "HumanName [text=" + text + "]";
	}
		
}
