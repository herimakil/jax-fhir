package es.udc.rs.app.model.util;

import java.util.List;

public class Organization
{
	private Identifier identifier;
	
	private boolean active;
	
	private enum Type { prov, dept, team, govt, ins, edu, reli, crs, cg, bus, other };
    private Type type;
	
    private String name;
    
	private List<String> alias;
	
	private ContactPoint telecom;
	
    private Address address;

    private String partOf;

    private Contact contact;

    private String endpoint;

	public Organization(Identifier identifier, boolean active, String type, String name, List<String> alias,
			ContactPoint telecom, Address address, String partOf, Contact contact, String endpoint) {
		super();
		this.identifier = identifier;
		this.active = active;
		this.type = Organization.Type.valueOf(type);
		this.name = name;
		this.alias = alias;
		this.telecom = telecom;
		this.address = address;
		this.partOf = partOf;
		this.contact = contact;
		this.endpoint = endpoint;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getAlias() {
		return alias;
	}

	public void setAlias(List<String> alias) {
		this.alias = alias;
	}

	public ContactPoint getTelecom() {
		return telecom;
	}

	public void setTelecom(ContactPoint telecom) {
		this.telecom = telecom;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPartOf() {
		return partOf;
	}

	public void setPartOf(String partOf) {
		this.partOf = partOf;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((endpoint == null) ? 0 : endpoint.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((partOf == null) ? 0 : partOf.hashCode());
		result = prime * result + ((telecom == null) ? 0 : telecom.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Organization other = (Organization) obj;
		if (active != other.active)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (endpoint == null) {
			if (other.endpoint != null)
				return false;
		} else if (!endpoint.equals(other.endpoint))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (partOf == null) {
			if (other.partOf != null)
				return false;
		} else if (!partOf.equals(other.partOf))
			return false;
		if (telecom == null) {
			if (other.telecom != null)
				return false;
		} else if (!telecom.equals(other.telecom))
			return false;
		if (type != other.type)
			return false;
		return true;
	}  

    
}
