package es.udc.rs.app.model.patient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//import java.sql.Timestamp;
//import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.udc.rs.app.exceptions.InstanceNotFoundException;
import es.udc.rs.app.model.util.Contact;
import es.udc.rs.app.model.util.HumanName;


public abstract class AbstractSqlPatientDao implements SqlPatientDao{
	
	protected AbstractSqlPatientDao(){
		
	}

	@Override
	public Patient update(Connection connection, Patient patient) throws InstanceNotFoundException {
		String queryString = "UPDATE Patient"
                + " SET name = ? WHERE patientId = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {

            /* Fill "preparedStatement". */
            int i = 1;
            preparedStatement.setString(i++, patient.getName());
//            Timestamp date = patient.getDate() != null ? new Timestamp(patient.getDate().getTime().getTime()) : null;
//            preparedStatement.setTimestamp(i++, date);
            preparedStatement.setLong(i++, patient.getPatientId());
            
            /* Execute query. */
            int updatedRows = preparedStatement.executeUpdate();

            if (updatedRows == 0) {
                throw new InstanceNotFoundException(patient.getPatientId(),
                        Patient.class.getName());
            }

            
            return patient;
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
	}

	@Override
	public void remove(Connection connection, String patientId) throws InstanceNotFoundException {
		/* Create "queryString". */
		String queryString = "DELETE FROM Patient WHERE "
				+ "patientId = ?";
		
		try (PreparedStatement preparedStatement = connection
				.prepareStatement(queryString)) {
			
			/* Fill "preparedStatement". */
			int i = 1;
			preparedStatement.setString(i++, patientId);
			
			/* Execute query. */
			int removedRows = preparedStatement.executeUpdate();

			if (removedRows == 0) {
				throw new InstanceNotFoundException(patientId,
						Patient.class.getName());
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Patient find(Connection connection, String patientId) throws InstanceNotFoundException {
		/* Create "queryString". */
		String queryString = "SELECT text, family, given1, given2, active, telecom, gender, birthdate, "
				+ "deceased, contactrel1, contact1, contactrel2, contact2"
				+ " FROM Patient WHERE patientId = ?";

		try (PreparedStatement preparedStatement = connection
				.prepareStatement(queryString)) {

			/* Fill "preparedStatement". */
			int i = 1;
			preparedStatement.setString(i++, patientId);

			/* Execute query. */
			ResultSet resultSet = preparedStatement.executeQuery();

			if (!resultSet.next()) {
				throw new InstanceNotFoundException(patientId,
						Patient.class.getName());
			}

			/* Get results. */
			String text = resultSet.getString("text");
			String family = resultSet.getString("family");
			String given1 = resultSet.getString("given1");
			String given2 = resultSet.getString("given2");
			
			List<String> givens = new ArrayList<String>();
			givens.add(given1);
			if (given2 != null){
				givens.add(given2);
			}
			HumanName name = new HumanName(family, givens);
			
			Boolean active = resultSet.getBoolean("active");
			String telecom = resultSet.getString("telecom");
			String gender = resultSet.getString("gender");
			Date birthDate = resultSet.getDate("birthdate");
			Boolean deceasedBoolean = resultSet.getBoolean("deceased");
			
			String contactRelationship1 = resultSet.getString("contactrel1");
			String contact1 = resultSet.getString("contact1");
			Contact firstContact = new Contact(contactRelationship1, contact1);
			
			String contactRelationship2 = resultSet.getString("contactrel2");
			String contact2 = resultSet.getString("contact2");
			Contact secondContact = null;
			if (contact2 != null){
				secondContact = new Contact(contactRelationship2, contact2); 
			}
			List<Contact> contacts = new ArrayList<Contact>();
			contacts.add(firstContact);
			if (secondContact != null){
				contacts.add(secondContact);
			}
			
			
			/* Return event. */
			return new Patient(patientId, name, active, telecom, gender, birthDate, deceasedBoolean, contacts);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
