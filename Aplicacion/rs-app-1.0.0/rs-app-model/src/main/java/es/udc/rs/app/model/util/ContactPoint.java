package es.udc.rs.app.model.util;

public class ContactPoint
{

//    private enum System { phone, fax, email, pager, url, sms, other };
    
//    private System system;
    
    private String value;

//    private enum Use{ home, work, temp, old, mobile};
//    private Use use;
	
//  private Integer rank;

//  private Period period;
    
    public ContactPoint(String value) {
		super();
//		this.system = System.valueOf(system);
		this.value = value;
//		this.use = Use.valueOf(use);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactPoint other = (ContactPoint) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContactPoint [value=" + value + "]";
	}

}