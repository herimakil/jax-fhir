package es.udc.rs.app.model.util;

public class ModelConstants {
	
	public static final String EVENT_DATA_SOURCE = "rs-app-ds";
	   
    private ModelConstants() {
    }

}
