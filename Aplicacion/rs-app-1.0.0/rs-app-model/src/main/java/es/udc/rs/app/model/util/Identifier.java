package es.udc.rs.app.model.util;

import java.net.URI;

public class Identifier
{

//    private URI system;

    private String value;

//    private enum Use { usual, official, temp, secondary };
//    private Use use;

//    private Organization assigner;

//    private enum Type { DL, PPN, BRN, MR, MCN, EN, TAX, NIIP, PRN, MD, DR, ACSN };
//    private Type type;

//    private Period period;

	public Identifier(String value) {
		super();
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identifier other = (Identifier) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Identifier [value=" + value + "]";
	}
}
