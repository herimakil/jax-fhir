package es.udc.rs.app.model.patient;

import java.sql.Connection;

import es.udc.rs.app.exceptions.InstanceNotFoundException;

public interface SqlPatientDao {
	
	public Patient create (Connection connection, Patient patient);
	
	public Patient update (Connection connection, Patient patient) throws InstanceNotFoundException;
	
	public void remove (Connection connection, String patientId) throws InstanceNotFoundException;
	
	public Patient find (Connection connection, String string) throws InstanceNotFoundException;
	
}
