package es.udc.rs.app.model.util;

public class Communication {
	private enum Language { hy, zh, nl, en, eo, fr, ka, de, el, 
		it, ja, ko, ku, fa, pl, pt, ro, ru, es, sv, tr, ur };
	private Language language;
	private boolean preferred;
	
	public Communication(String language, boolean preferred){
		this.language = Communication.Language.valueOf(language);
		this.preferred = preferred;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isPreferred() {
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}

	@Override
	public String toString() {
		return "Communication [language=" + language + ", preferred=" + preferred + "]";
	}

}
