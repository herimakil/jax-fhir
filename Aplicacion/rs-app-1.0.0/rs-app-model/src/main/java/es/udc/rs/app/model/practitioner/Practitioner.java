package es.udc.rs.app.model.practitioner;

import java.util.Date;
import java.util.List;

import es.udc.rs.app.model.util.Address;
import es.udc.rs.app.model.util.Communication;
import es.udc.rs.app.model.util.ContactPoint;
import es.udc.rs.app.model.util.Gender;
import es.udc.rs.app.model.util.HumanName;
import es.udc.rs.app.model.util.Identifier;
import es.udc.rs.app.model.util.Photo;
import es.udc.rs.app.model.util.Qualification;

public class Practitioner
{
    private Identifier identifier;
    
    private List<Qualification> qualification;

    private List<Address> address;

    private List<Communication> communication;

    private HumanName name;

    private ContactPoint telecom;

    private boolean active;

    private Gender gender;

    private Date birthDate;

    private Photo photo;

	public Practitioner(Identifier identifier, List<Qualification> qualification, List<Address> address,
			List<Communication> communication, HumanName name, ContactPoint telecom, boolean active, Gender gender,
			Date birthDate, Photo photo) {
		super();
		this.identifier = identifier;
		this.qualification = qualification;
		this.address = address;
		this.communication = communication;
		this.name = name;
		this.telecom = telecom;
		this.active = active;
		this.gender = gender;
		this.birthDate = birthDate;
		this.photo = photo;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public List<Qualification> getQualification() {
		return qualification;
	}

	public void setQualification(List<Qualification> qualification) {
		this.qualification = qualification;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public List<Communication> getCommunication() {
		return communication;
	}

	public void setCommunication(List<Communication> communication) {
		this.communication = communication;
	}

	public HumanName getName() {
		return name;
	}

	public void setName(HumanName name) {
		this.name = name;
	}

	public ContactPoint getTelecom() {
		return telecom;
	}

	public void setTelecom(ContactPoint telecom) {
		this.telecom = telecom;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}
    
    
    
    

}
