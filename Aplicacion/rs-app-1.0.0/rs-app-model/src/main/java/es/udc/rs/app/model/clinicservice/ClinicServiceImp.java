package es.udc.rs.app.model.clinicservice;

import static es.udc.rs.app.model.util.ModelConstants.EVENT_DATA_SOURCE;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.sql.DataSource;

import es.udc.rs.app.exceptions.InputValidationException;
import es.udc.rs.app.exceptions.InstanceNotFoundException;
import es.udc.rs.app.model.patient.Patient;
import es.udc.rs.app.model.patient.SqlPatientDao;
import es.udc.rs.app.model.patient.SqlPatientDaoFactory;
import es.udc.rs.app.sql.DataSourceLocator;


public class ClinicServiceImp implements ClinicService{
	
	private DataSource dataSource;
	private SqlPatientDao patientDao = null;

	public ClinicServiceImp() {
		dataSource = DataSourceLocator.getDataSource(EVENT_DATA_SOURCE);
		patientDao = SqlPatientDaoFactory.getDao();

	}

	public static String calendarToString(Calendar date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd/MM/yyyy HH:mm");
		return simpleDateFormat.format(date.getTime());
	}
	
	public static void validatePatient(Patient patient) throws InputValidationException {

		if ((patient.getName() == null) || patient.getName().getText().isEmpty()) {
			throw new InputValidationException("Patient has no name");
		}
	}

	@Override
	public Patient addPatient(Patient patient) throws InputValidationException {
		try (Connection connection = dataSource.getConnection()) {

			try {
				validatePatient(patient);
				/* Prepare connection. */
				connection
						.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
				connection.setAutoCommit(false);

				/* Do work. */
				Patient createdPatient = patientDao.create(connection, patient);

				/* Commit. */
				connection.commit();

				return createdPatient;

			} catch (SQLException e) {
				connection.rollback();
				throw new RuntimeException(e);
			} catch (RuntimeException | Error e) {
				connection.rollback();
				throw e;
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void updatePatient(Patient patient) throws InstanceNotFoundException, InputValidationException {
		try (Connection connection = dataSource.getConnection()) {

			try {
				validatePatient(patient);
				
				/* Prepare connection. */
				connection
						.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
				connection.setAutoCommit(false);

				Patient antPatient = patientDao.find(connection, patient.getIdentifier().getValue());
				
				if (antPatient == null) {
					throw new InstanceNotFoundException(connection, Patient.class.getName());
				}
				
				patient.setIdentifier(antPatient.getIdentifier());

				/* Do work. */
				patientDao.update(connection, patient);

				/* Commit. */
				connection.commit();
			} catch (SQLException e) {
				connection.rollback();
				throw new RuntimeException(e);
			} catch (RuntimeException | Error e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public void removePatient(String patientId) throws InstanceNotFoundException {
		try (Connection connection = dataSource.getConnection()) {

			try {

				/* Prepare connection. */
				connection
						.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
				connection.setAutoCommit(false);

				/* Tenemos que ver que no haya nadie apuntado ya a ese evento. */

				Patient patientToRemove = patientDao.find(connection, patientId);
				if (patientToRemove == null){
					throw new InstanceNotFoundException(connection, Patient.class.getName());
				}

				/* Do work. */
				patientDao.remove(connection, patientId);

				/* Commit. */
				connection.commit();

			} catch (InstanceNotFoundException e) {
				connection.commit();
				throw e;
			} catch (SQLException e) {
				connection.rollback();
				throw new RuntimeException(e);
			} catch (RuntimeException | Error e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Patient findPatient(String patientId) throws InstanceNotFoundException {
		try (Connection connection = dataSource.getConnection()) {
			Patient patient = patientDao.find(connection, patientId);
			return patient;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
