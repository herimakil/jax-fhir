package es.udc.rs.app.jaxr.util;

import java.util.ArrayList;
import java.util.List;

import es.udc.rs.app.jaxrs.dto.ContactDtoJaxb;
import es.udc.rs.app.model.util.Contact;

public class ContacConversor {
	
	public static ContactDtoJaxb toContactDtoJaxb(Contact contact){
		return new ContactDtoJaxb(contact.getRelationship().toString(), contact.getValue());
	}
	
	public static Contact toContact(ContactDtoJaxb contact){
		return new Contact(contact.getRelationship(), contact.getValue());
	}
	
	public static List<ContactDtoJaxb> toContactListDtoJaxb(List<Contact> contact){
		List<ContactDtoJaxb> contactsReturn = new ArrayList<>(contact.size());
		for(int i = 0; i < contact.size(); i++){
			Contact contactToAdd = contact.get(i);
			contactsReturn.add(toContactDtoJaxb(contactToAdd));
		}
		return contactsReturn;
	}
	
	public static List<Contact> toContactList(List<ContactDtoJaxb> contact){
		List<Contact> contactsReturn = new ArrayList<>(contact.size());
		for(int i = 0; i < contact.size(); i++){
			ContactDtoJaxb contactToAdd = contact.get(i);
			contactsReturn.add(toContact(contactToAdd));
		}
		return contactsReturn;
	}
	

}
