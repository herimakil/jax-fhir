package es.udc.rs.app.jaxrs.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import es.udc.rs.app.jaxr.util.ContacConversor;
import es.udc.rs.app.model.util.Contact;
import es.udc.rs.app.model.util.ContactPoint;
import es.udc.rs.app.model.util.Gender;
import es.udc.rs.app.model.util.HumanName;
import es.udc.rs.app.model.util.Identifier;

@XmlRootElement(name = "Patient")
@XmlType(name = "patientType", propOrder = {"identifier", "name", "active", "telecom", "gender", 
		"birthDate", "deceasedBoolean", "contact"})
public class PatientDtoJaxb {
	
	@XmlElement(name = "identifier", required = true)
	private Identifier identifier;
	@XmlElement(name = "name", required = true)
	private HumanName name;
	@XmlElement(name = "active", required = true)
	private boolean active;
	@XmlElement(name = "telecom", required = true)
	private ContactPoint telecom;
	@XmlElement(name = "gender", required = true)
	private Gender gender;
	@XmlElement(name = "birthDate", required = true)
	private Date birthDate;
	@XmlElement(name = "deceasedBoolean", required = true)
	private boolean deceasedBoolean;
	@XmlElement(name = "contact", required = true)
	private List<ContactDtoJaxb> contact;
	
	public PatientDtoJaxb() {
		
	}
	
	public PatientDtoJaxb(Identifier identifier, HumanName name, boolean active, ContactPoint telecom, 
			Gender gender, Date birthDate, boolean deceasedBoolean, List<Contact> contact){
		this.identifier = identifier;
		this.name = name;
		this.active = active;
		this.telecom = telecom;
		this.gender = gender;
		this.birthDate = birthDate;
		this.deceasedBoolean = deceasedBoolean;
		this.contact = ContacConversor.toContactListDtoJaxb(contact);
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public HumanName getName() {
		return name;
	}

	public void setName(HumanName name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ContactPoint getTelecom() {
		return telecom;
	}

	public void setTelecom(ContactPoint telecom) {
		this.telecom = telecom;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public boolean isDeceasedBoolean() {
		return deceasedBoolean;
	}

	public void setDeceasedBoolean(boolean deceasedBoolean) {
		this.deceasedBoolean = deceasedBoolean;
	}

	public List<ContactDtoJaxb> getContact() {
		return contact;
	}

	public void setContact(List<ContactDtoJaxb> contact) {
		this.contact = contact;
	}
	
}
