package es.udc.rs.app.jaxr.util;

import java.util.Date;
import java.util.List;

import es.udc.rs.app.jaxrs.dto.PatientDtoJaxb;
import es.udc.rs.app.model.patient.Patient;
import es.udc.rs.app.model.util.Contact;
import es.udc.rs.app.model.util.ContactPoint;
import es.udc.rs.app.model.util.Gender;
import es.udc.rs.app.model.util.HumanName;
import es.udc.rs.app.model.util.Identifier;


public class PatientConversor {
	
	public static Patient toPatient(PatientDtoJaxb patientDto){
		return new Patient(patientDto.getIdentifier(), patientDto.getName(), patientDto.isActive(), 
				patientDto.getTelecom(), patientDto.getGender(), patientDto.getBirthDate(), 
				patientDto.isDeceasedBoolean(), ContacConversor.toContactList(patientDto.getContact()));
	}

	public static PatientDtoJaxb toPatientDtoJaxb(Patient patient) {
		return new PatientDtoJaxb(patient.getIdentifier(), patient.getName(), patient.isActive(),
				patient.getTelecom(), patient.getGender(), patient.getBirthDate(), patient.getDeceasedBoolean(),
				patient.getContact());
	}
}