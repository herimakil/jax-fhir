package es.udc.rs.app.jaxr.util;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

public class ServiceUtil {
	
	private static List<MediaType> responseMediaTypes = (List<MediaType>) Arrays
			.asList(new MediaType[] { MediaType.APPLICATION_JSON_TYPE,
					MediaType.APPLICATION_XML_TYPE });

	public static String getTypeAsStringFromHeaders(HttpHeaders headers) {
		List<MediaType> mediaTypes = headers.getAcceptableMediaTypes();
		for (MediaType m : mediaTypes) {
			MediaType compatibleType = getCompatibleAcceptableMediaType(m);
			if (compatibleType != null) {
				return compatibleType.toString();
			}
		}
		return null;
	}

	private static MediaType getCompatibleAcceptableMediaType(MediaType type) {
		for (MediaType m : responseMediaTypes) {
			if (m.isCompatible(type)) {
				return m;
			}
		}
		return null;
	}

}
