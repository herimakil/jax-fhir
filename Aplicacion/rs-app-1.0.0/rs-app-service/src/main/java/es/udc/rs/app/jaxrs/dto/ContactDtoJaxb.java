package es.udc.rs.app.jaxrs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Contact")
@XmlType(name = "contactType", propOrder = {"relationship", "value"})
public class ContactDtoJaxb {
	
	@XmlElement(name = "relationship", required = true)
	private String relationship;
	@XmlElement(name = "value", required = true)
    private String value;
    
    public ContactDtoJaxb(){
    	
    }
    
    public ContactDtoJaxb(String relationship, String value){
    	this.relationship = relationship;
    	this.value = value;
    }
    
	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
    
}
