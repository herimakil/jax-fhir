package es.udc.rs.app.jarxs.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


import es.udc.rs.app.jaxr.util.PatientConversor;
import es.udc.rs.app.jaxr.util.ServiceUtil;
import es.udc.rs.app.jaxrs.dto.PatientDtoJaxb;
import es.udc.rs.app.model.clinicservice.ClinicService;
import es.udc.rs.app.model.clinicservice.ClinicServiceFactory;
import es.udc.rs.app.model.patient.Patient;
import es.udc.ws.util.exceptions.InputValidationException;

@Path("/patient")
public class PatientResource {
	
	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response addPatient(PatientDtoJaxb patientDto, @Context HttpHeaders headers, @Context UriInfo uriInfo) 
			throws es.udc.rs.app.exceptions.InputValidationException{
		
		String type = ServiceUtil.getTypeAsStringFromHeaders(headers);
		ClinicService servicio = ClinicServiceFactory.getService();
		Patient patient = PatientConversor.toPatient(patientDto);
		try{
			patient = servicio.addPatient(patient);
		}catch (es.udc.rs.app.exceptions.InputValidationException e) {
			return Response.status(Response.Status.BAD_REQUEST)
					.entity(new InputValidationException(e.getMessage()))
					.type(type).build();
		}
		
		PatientDtoJaxb resultPatient = PatientConversor.toPatientDtoJaxb(patient);
		
		return Response.created(URI.create(uriInfo.getBaseUri().toString()+"patient/"+ 
				resultPatient.getIdentifier().toString())).entity(resultPatient).type(type).build();
		
	}
}
